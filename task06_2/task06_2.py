#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import typing as t

import redis
from dotenv import load_dotenv


def main(command_line: t.Optional[str] = None) -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument("key", action="store", help="the redis key name")
    parser.add_argument("limit", action="store", type=int, help="the number of items")

    args = parser.parse_args(command_line)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        values = r.lpop(args.key, args.limit)

    if values is not None:
        for value in values:
            print(value.decode("utf-8"))


if __name__ == "__main__":
    load_dotenv()

    main()
