# Условие задачи

Напишите программу, которая принимает в качестве первого аргумента значение $key, а второго — $limit, при запуске извлекает $limit элементов из начала списка в ключе $key и выводит их в stdout.