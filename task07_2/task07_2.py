#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import redis
from dotenv import load_dotenv


def main() -> None:
    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    prefix_size = len(b"data:")

    with redis.Redis(host=redis_host, port=redis_port) as r:
        for key in r.scan_iter("data:*"):
            size = r.strlen(key)
            print(f"{size} {key[prefix_size:].decode('utf-8')}")


if __name__ == "__main__":
    load_dotenv()

    main()
