#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import asyncio
import itertools
import os
import sys
import typing as t

import aioredis
from dotenv import load_dotenv

TIMEOUT: t.Final[int] = 5


async def read_values(
    redis_uri: str, key: str, counter: itertools.count, final: int, event: asyncio.Event
) -> None:
    redis = await aioredis.from_url(redis_uri, encoding="utf-8", decode_responses=True)
    async with redis.client() as conn:
        while not event.is_set():
            value = await conn.blpop(key, timeout=1)
            if value:
                print(f"{value[0]} {value[1]}")
                count = next(counter)
                if count >= final:
                    event.set()


async def main(command_line: t.Optional[str] = None) -> int:
    parser = argparse.ArgumentParser()

    parser.add_argument("count", action="store", type=int, help="the number of values")
    parser.add_argument("keys", action="store", nargs="+", help="the keys list")

    args = parser.parse_args(command_line)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = os.getenv("REDIS_PORT", "6379")
    redis_uri = f"redis://{redis_host}:{redis_port}"

    counter = itertools.count()
    _ = next(counter)
    event = asyncio.Event()

    tasks = [
        read_values(redis_uri, key, counter, args.count, event) for key in args.keys
    ]
    watchdog = asyncio.wait_for(event.wait(), timeout=TIMEOUT)

    try:
        await asyncio.gather(*tasks, watchdog, return_exceptions=False)
        return 0

    except asyncio.TimeoutError:
        event.set()
        await asyncio.gather(*tasks, return_exceptions=True)
        return 1


if __name__ == "__main__":
    load_dotenv()

    loop = asyncio.new_event_loop()
    sys.exit(loop.run_until_complete(main()))
