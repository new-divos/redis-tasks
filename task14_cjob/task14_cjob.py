#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import typing as t
from pathlib import Path

import redis
from dotenv import load_dotenv


def main(command_line: t.Optional[str] = None) -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument("ns", action="store", help="The Redis keys prefix")
    parser.add_argument(
        "files", action="store", nargs="+", type=Path, help="The file names"
    )

    args = parser.parse_args(command_line)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    job_key = f"{args.ns}:job"
    rest = [str(path) for path in args.files]

    with redis.Redis(host=redis_host, port=redis_port) as r:
        r.lpush(job_key, *(path for path in set(rest)))

        while len(rest) > 0:
            while True:
                job_values = [
                    (path, value)
                    for path, value in zip(
                        rest, r.mget(f"{args.ns}:res:{path}" for path in rest)
                    )
                    if value
                ]

                if job_values:
                    for path, value in job_values:
                        print(f"{path} {value.decode('utf-8')}")
                        rest.remove(path)

                    break

                else:
                    mark = r.blpop(f"{args.ns}:res", 3)
                    if mark is None:
                        print("The data is not ready ...", file=sys.stderr)


if __name__ == "__main__":
    load_dotenv()

    main()
