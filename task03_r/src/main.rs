use std::{env, error, fmt};

use anyhow::{Context, Result};
use dotenv::dotenv;
use redis::{self, Commands};

#[derive(Debug)]
struct AppError(&'static str);

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, file: {}, line: {}", self.0, file!(), line!())
    }
}

impl error::Error for AppError {}

fn main() -> Result<()> {
    dotenv().ok();

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage:\n\ttask03_r ns arg1 [arg2 arg3 ... argn]");
        return Err(AppError("Illegal command line"))
            .context("Illegal arguments in command line")?;
    }

    let redis_url = match env::var("REDIS_URL") {
        Ok(value) => value,
        Err(_) => "redis://localhost:6379/".to_string(),
    };

    let client = redis::Client::open(redis_url)
        .context("Failed to open Redis connection")?;
    let mut con = client.get_connection()
        .context("Failed to retrieve Redis connection")?;

    let ns = &args[1];
    let id: usize = con.incr(ns.as_str(), 1)
        .context("Failed to increment value")?;

    for value in args.iter().skip(2) {
        let key = format!("{}-{}-{}", ns, id, value);
        let _: () = con.set(key, value.as_str())
            .context("Failed to set value")?;
    }
    
    Ok(())
}
