#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import io
import random
import sys
import typing as t
from pathlib import Path

import redis

from task06_1 import LIST_LENGTH, process


def get_key() -> str:
    key = list("abcdefghijklmnopqrstuvwxwz0123456789")
    random.shuffle(key)
    return "".join(key)


def test_file(connection: redis.Redis) -> None:
    path = Path(__file__).parent.joinpath("assets", "the_raven.txt")

    key = get_key()
    with open(path, "r", encoding="utf-8") as fin:
        process(key, fin)

    assert connection.llen(key) == LIST_LENGTH

    redis_lines = [arr.decode("utf-8") for arr in connection.lrange(key, 0, -1)]

    file_lines = []
    with open(path, "r", encoding="utf-8") as fin:
        for line in fin:
            file_lines.append(line.rstrip())

    for redis_line, file_line in zip(redis_lines, file_lines[-LIST_LENGTH:]):
        assert redis_line == file_line

    connection.delete(key)


def test_stdin(connection: redis.Redis, monkeypatch: t.Any) -> None:
    lines = [f"line {i}" for i in range(1, 2 * LIST_LENGTH)]
    monkeypatch.setattr("sys.stdin", io.StringIO("\n".join(lines)))

    key = get_key()
    process(key, sys.stdin)

    assert connection.llen(key) == LIST_LENGTH

    redis_lines = [arr.decode("utf-8") for arr in connection.lrange(key, 0, -1)]

    for redis_line, line in zip(redis_lines, lines[-LIST_LENGTH:]):
        assert redis_line == line

    connection.delete(key)
