#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import typing as t
from pathlib import Path

import redis

from task07_1 import process

TEMP_DEPTH: t.Final[int] = 3
TEMP_COUNT: t.Final[int] = 5
TEMP_SIZE: t.Final[int] = 8192

TEMP_WIDTH: t.Final[int] = 100


def test_temp(
    connection: redis.Redis, temptree: t.Callable[[int, int, int], Path]
) -> None:
    root = temptree(TEMP_DEPTH, TEMP_COUNT, TEMP_SIZE)
    process(root, connection, TEMP_WIDTH)

    def check_files(path: Path):
        for child in path.iterdir():
            if child.is_dir():
                check_files(child)

            elif child.is_file():
                with open(child, "rb") as fin:
                    content = fin.read()

                key = f"data:{child.as_posix()}"
                data = connection.get(key)

                assert data == content

    check_files(root)

    for key in connection.scan_iter(f"data:{root.as_posix()}/*"):
        connection.delete(key)
