#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import typing as t

import redis

from task08 import process

ITERATIONS: t.Final[int] = 200
MIN_VALUE: t.Final[int] = -10000
MAX_VALUE: t.Final[int] = 10000


def test_incr(connection: redis.Redis) -> None:
    key_lst = list("01234567890abcdefghijklmnopqrstuvwxyz")
    random.shuffle(key_lst)
    key = "".join(key_lst)

    value = 0
    for _ in range(ITERATIONS):
        number = random.randint(MIN_VALUE, MAX_VALUE)

        process(connection, key, number)

        value += number
        stored = int(connection.get(key))  # type: ignore
        assert stored == value

    connection.delete(key)
