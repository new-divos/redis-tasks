#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import random
import tempfile
import typing as t
from pathlib import Path

import pytest
import redis
from dotenv import load_dotenv


@pytest.fixture(scope="session")
def connection(request: pytest.FixtureRequest) -> redis.Redis:
    load_dotenv()

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    r = redis.Redis(host=redis_host, port=redis_port)

    def redis_finalizer() -> None:
        r.close()

    request.addfinalizer(redis_finalizer)
    return r


@pytest.fixture
def temptree(request: pytest.FixtureRequest) -> t.Callable[[int, int, int], Path]:
    def randname() -> str:
        letters: t.List[str] = list(
            "0123456789_-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        )

        random.shuffle(letters)
        return "".join(letters[: random.randint(1, len(letters) - 1)])

    root = Path(tempfile.gettempdir()).joinpath("temptree", randname())

    def build_tree(depth: int, count: int, size: int) -> Path:
        def add_files(path: Path, depth: int) -> None:
            if depth == 0:
                for _ in range(count):
                    child = path / f"{randname()}.bin"
                    content = random.randbytes(random.randint(10, size))

                    with open(child, "wb") as fout:
                        fout.write(content)

            else:
                for _ in range(count):
                    child = path / randname()
                    child.mkdir(parents=True, exist_ok=True)

                    add_files(child, depth - 1)

        add_files(root, depth)
        return root

    def remove_root() -> None:
        def remove_tree(path: Path) -> None:
            for child in path.iterdir():
                if child.is_file():
                    child.unlink()
                else:
                    remove_tree(child)

            path.rmdir()

        if root.exists():
            remove_tree(root)

    request.addfinalizer(remove_root)
    return build_tree
