#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from task09_pub import get_key


def test_key():
    assert get_key("mykey", 982668) == "mykey:0010002021"
    assert get_key("mykey", 2689) == "mykey:0010001011"
    assert get_key("mykey", 4242424242) == "mykey:0050500000"
