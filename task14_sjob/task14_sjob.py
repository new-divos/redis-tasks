#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hashlib
import os
import sys
import typing as t

import redis
from dotenv import load_dotenv


def main(args: t.List[str]) -> None:
    if len(args) != 1:
        print("Usage:\n\tpython task14_sjob.py ns", file=sys.stderr)
        sys.exit(1)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    job_key = f"{args[0]}:job"
    with redis.Redis(host=redis_host, port=redis_port) as r:
        r.expire(f"{args[0]}:res", 10)

        try:
            while True:
                path_bytes: t.Optional[bytes] = r.brpoplpush(job_key, job_key, 1)
                if path_bytes is not None:
                    path = path_bytes.decode("utf-8")
                    with open(path, "rb") as fin:
                        sha1 = hashlib.sha1(fin.read()).hexdigest()

                    pipeline = r.pipeline()
                    pipeline.set(f"{args[0]}:res:{path}", sha1)
                    pipeline.lpush(f"{args[0]}:res", "ready")
                    pipeline.lrem(job_key, 1, path)

                    pipeline.execute()

        except KeyboardInterrupt:
            print("done")


if __name__ == "__main__":
    load_dotenv()

    main(sys.argv[1:])
