#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import typing as t
from pathlib import Path

import redis
from dotenv import load_dotenv

LIST_LENGTH: t.Final[int] = 20


def process(key: str, stream: t.TextIO) -> None:
    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        for line in stream:
            r.rpush(key, line.rstrip())

            length = r.llen(key)
            if length > LIST_LENGTH:
                r.lpop(key, length - LIST_LENGTH)


def main(command_line: t.Optional[str] = None) -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        "-i",
        dest="path",
        type=Path,
        required=False,
        action="store",
        default=None,
        help="input file path, if not specified the stdin will be used",
    )
    parser.add_argument("key", action="store", help="the redis key name")

    args = parser.parse_args(command_line)

    if args.path is not None:
        path: Path = args.path
        if path.exists() and path.is_file():
            with open(path, "r", encoding="utf-8") as fin:
                process(args.key, fin)
        else:
            print(f"Illegal input file path {path}", file=sys.stderr)
    else:
        process(args.key, sys.stdin)


if __name__ == "__main__":
    load_dotenv()

    main()
