#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .task06_1 import LIST_LENGTH, process

__all__ = ["LIST_LENGTH", "process"]
