#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import typing as t
from pathlib import Path

import redis
from dotenv import load_dotenv


def process(path: Path, connection: redis.Redis, width: int) -> None:
    def crawl(path: Path, pipeline: redis.client.Pipeline, count: int) -> int:
        if path.is_dir():
            for child in path.iterdir():
                count = crawl(child, pipeline, count)

        elif path.is_file():
            try:
                with open(path, "rb") as fin:
                    content = fin.read()

                key = f"data:{path.as_posix()}"
                pipeline.set(key, content)

            except Exception:
                return count

            count += 1
            if count > width:
                pipeline.execute()
                count = 0

        return count

    pipeline = connection.pipeline(transaction=False)

    count = 0
    for key in connection.scan_iter(f"data:{path.as_posix()}/*"):
        pipeline.delete(key)
        count += 1

    count = crawl(path, pipeline, count)
    if count > 0:
        pipeline.execute()


def main(command_line: t.Optional[str] = None):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "root",
        action="store",
        type=Path,
        help="the toot directory to crawl",
    )
    parser.add_argument(
        "--width",
        "-w",
        action="store",
        type=int,
        default=1000,
        help="the pipeline size",
    )

    args = parser.parse_args(command_line)

    root: Path = args.root
    root = root.absolute()

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        process(root, r, args.width)


if __name__ == "__main__":
    load_dotenv()

    main()
