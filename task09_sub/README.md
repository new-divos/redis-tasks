# Условие задачи

Напишите программу, которая принимает аргумент $name и использует его для формирования подписки таким образом, чтобы получить и вывести одно случайное число, в котором цифра цифра 7 встречается не меньше 3 раз, при этом не должно быть других нечётных цифр.

- Ключ cmd-7777 должен содержать команду для запуска этой программы.
- Примечание: задание будет считаться выполненным, если будет удовлетворять поставленному условию. Но высший балл вы получите только в случае, если решите её таким образом, чтобы не было обработано ни одного лишнего сообщения: в теоретической части для этого есть всё необходимое.