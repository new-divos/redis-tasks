#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import typing as t

import redis
from dotenv import load_dotenv


def main(args: t.List[str]) -> None:
    if len(args) < 1:
        print("Usage:\n\tpython task09_sub.py name\n", file=sys.stderr)
        sys.exit(1)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    name = args[0]
    try:
        with redis.Redis(host=redis_host, port=redis_port) as r:
            p = r.pubsub()
            p.psubscribe(f"{name}:?0?0?0?[3-9]?0")

            while True:
                message = p.get_message()
                if message and message.get("type") == "pmessage":
                    data: t.Optional[bytes] = message.get("data")
                    if data is not None:
                        print(data.decode("utf-8"))
                        break

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    load_dotenv()

    main(sys.argv[1:])
