#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import typing as t

import redis
from dotenv import load_dotenv


def main(args: t.List[str]) -> None:
    if len(args) < 2:
        print(
            "Usage:\n\tpython task03.py ns arg1 [arg2 arg3 ... argn]", file=sys.stderr
        )
        sys.exit(1)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        ns = args[0]
        id = r.incr(ns)

        for idx, value in enumerate(args[1:]):
            r.set(f"{ns}-{id}-{idx}", value)


if __name__ == "__main__":
    load_dotenv()

    main(sys.argv[1:])
