#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .task09_pub import get_key

__all__ = ["get_key"]
