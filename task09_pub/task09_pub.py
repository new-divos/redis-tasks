#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import itertools
import os
import random
import sys
import typing as t
from collections import OrderedDict

import redis
from dotenv import load_dotenv


def get_key(name: str, number: int) -> str:
    digits = OrderedDict(zip(range(10), itertools.repeat(0, 10)))

    if number == 0:
        digits[0] += 1

    else:
        value = number
        while value != 0:
            digit = value % 10
            value //= 10

            digits[digit] += 1

    s = "".join([str(value) for value in digits.values()])
    return f"{name}:{s}"


def main(args: t.List[str]) -> None:
    if len(args) < 1:
        print("Usage:\n\tpython task09_pub.py name\n", file=sys.stderr)
        sys.exit(1)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    name = args[0]
    try:
        with redis.Redis(host=redis_host, port=redis_port) as r:
            while True:
                number = random.randint(0, 999999999)
                r.publish(get_key(name, number), str(number))

    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    load_dotenv()

    main(sys.argv[1:])
