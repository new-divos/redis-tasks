#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .task08 import process

__all__ = ["process"]
