#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import typing as t

import redis
from dotenv import load_dotenv


def process(connection: redis.Redis, key: str, number: int) -> None:
    pipeline: redis.client.Pipeline = connection.pipeline(transaction=True)
    pipeline.watch(key)

    value = pipeline.get(key)

    pipeline.multi()
    try:
        if value is None:
            pipeline.set(key, str(number))

        else:
            pipeline.set(key, str(int(value) + number))  # type: ignore

    finally:
        pipeline.execute()


def main(command_line: t.Optional[str] = None) -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument("key", action="store", help="the value key")
    parser.add_argument(
        "number", action="store", type=int, help="the number to increment value"
    )

    args = parser.parse_args(command_line)

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        process(r, args.key, args.number)


if __name__ == "__main__":
    load_dotenv()

    main()
