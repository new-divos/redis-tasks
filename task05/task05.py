#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import base64
import json
import os
import typing as t
from pathlib import Path

import redis
from dotenv import load_dotenv

CHUNK_SIZE: t.Final[int] = 65536


def main(command_line: t.Optional[str] = None) -> None:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--bytes",
        "-c",
        type=int,
        action="store",
        default=100,
        help="number of the last output bytes",
    )
    parser.add_argument(
        "file", type=Path, action="store", help="path to the input file"
    )

    args = parser.parse_args(command_line)

    path = args.file.absolute()
    count = args.bytes

    redis_host = os.getenv("REDIS_HOST", "localhost")
    redis_port = int(os.getenv("REDIS_PORT", "6379"))

    with redis.Redis(host=redis_host, port=redis_port) as r:
        key = "task05:{}".format(
            base64.b64encode(
                json.dumps(dict(path=str(path), count=count)).encode("utf-8")
            ).decode("latin1")
        )

        result: t.Optional[bytes] = r.get(key)
        if result is None:
            chunk_size = max(CHUNK_SIZE, count)
            curr_chunk, prev_chunk = None, None
            with open(path, "rb") as fin:
                while True:
                    chunk = fin.read(chunk_size)
                    if not chunk:
                        break

                    prev_chunk, curr_chunk = curr_chunk, chunk

            if curr_chunk is None:
                result = b""

            elif len(curr_chunk) < count:
                if prev_chunk is None:
                    result = curr_chunk
                else:
                    diff = count - len(curr_chunk)
                    result = prev_chunk[-diff:] + curr_chunk

            else:
                result = curr_chunk[-count:]

            r.set(key, result)

        print(result.decode("latin1"), end="")


if __name__ == "__main__":
    load_dotenv()

    main()
